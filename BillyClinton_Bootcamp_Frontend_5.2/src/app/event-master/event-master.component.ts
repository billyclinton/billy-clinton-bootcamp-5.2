import { Component, OnInit } from '@angular/core';
import { EventService } from '../service/event.service';

@Component({
  selector: 'app-event-master',
  templateUrl: './event-master.component.html',
  styleUrls: ['./event-master.component.css']
})
export class EventMasterComponent implements OnInit {

  constructor(private event:EventService) { }

  event_id;
  event_name;
  date_and_time;
  ticket_price;
  total_ticket;
  ticket_sold;
  ticket_remaining;
  ticket_status;
  
  ngOnInit() {
  }

   addEvent(){
    this.event.addEvent(this.event_id,this.event_name,this.date_and_time,this.ticket_price,this.total_ticket,this.ticket_sold,this.ticket_remaining,this.ticket_status);
  }
}
