import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class EventService {

 constructor(private http:Http) { 
  }

  getlist(){
    this.http.get('http://localhost:8000/api/list')
    .map(res=>res.json())
    .catch(error => Observable.throw(error.json().error) || "Server Error")
    .subscribe(result => this.eventlist = result);
  }
 eventlist:Object[];
  addEvent(event_id,event_name,date_and_time,ticket_price,total_ticket,ticket_sold,ticket_remaining,ticket_status){
    let data = {
      "event_name" : event_name,
      "date_and_time" : date_and_time,
      "ticket_price" : ticket_price,
      "total_ticket" : total_ticket,
      "ticket_sold" : ticket_sold,
      "ticket_remaining" : ticket_remaining,
      "ticket_status" : ticket_status
    };

    let body = JSON.stringify(data);
    let headers = new Headers({"Content-Type" : "application/json"});
    let options = new RequestOptions ({headers:headers});
    this.http.post('http://localhost:8000/api/addEvent',body,options);
  }

}
