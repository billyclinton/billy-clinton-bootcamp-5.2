import { Component, OnInit } from '@angular/core';
import { EventService } from '../service/event.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {
  event_name; 
  date_and_time;
  ticket_price;
  total_ticket;
  ticket_sold;
  ticket_remaining;
  ticket_status;

  eventlist:Object[];


  constructor(private event:EventService) { }


  ngOnInit() {
    this.event.getlist().subscribe(result => this.eventlist = result);
  }

}
