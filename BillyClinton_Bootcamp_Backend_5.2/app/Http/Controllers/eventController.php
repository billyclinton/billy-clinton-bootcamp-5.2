<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\event;

class eventController extends Controller
{
        function getList()
    {
        return this.http.get('http://localhost:8000/api/getList')
        .map(result=>result.json());
    }

        function addEvent(request $request)
    {
        DB::beginTransaction();
        try{
            $event= new event;
            $event->event_name = $request->input('event_name');
            $event->date_and_time= $request->input('date_and_time');
            $event->ticket_price= $request->input('ticket_price');
            $event->total_ticket= $request->input('total_ticket');
            $event->ticket_remaining= $request->input('ticket_remaining');
            $event->ticket_sold= $request->input('ticket_sold');
            $event->ticket_status= $request->input('ticket_status');
            $event->save();
            return response()->json( $event , 201);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }
}

